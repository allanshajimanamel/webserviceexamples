# WebServiceExamples #

The repository contains my practice implementation for SOAP based and REST based webservices. The webservices created are used to perform create, read and update operations on a Person entity. A person entity has an id, name and age.

The project structure was created using Maven.
1. The main project here is webservices which build the whole project and updates the EAR.
2. The project webservices-server contains the backened implementation. I used EJB 3.1 and JPA 2.0 for the implementation.
3. The project webservices-api contains the interface to the project webservices-server. This was done so that the webservice and backend can be developed independently of each other and also provide of distributed deployment.
4. The project webservices-ear is the ear for the implementation.
5. The project webservices-soap contains the SOAP based web service.
6. The project webservices-rest contains the rest based web service.

I use **Wildfly 8.2** as my application server and **MySQL 5** as my database server.

For the SOAP based web service I am using **Apache CXF** to create the web service in a bottom up approach. Since I am using **Wildfly 8.2**, I am using the Apache CXF library provided by the JBoss ws which is inbuilt into the server. If we use our own Apache CXF implementation we will get class loading exceptions. If we are to build it for any other server, like Tomcat, then we have to provide the implementation of Apache CXF and generate the wsdl from code.
Since Wildfly 8.2 has a full fledged J2EE container with JBoss ws we do not generate the wsdl or schema for the messages. It will be created by JBoss when we deploy the application using its provided Apache CXF library.

For the REST based web service I am using the **JBoss RestEasy API** and **Jackson API**.